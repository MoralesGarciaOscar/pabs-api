require('dotenv').config();
const ENV = process.env; // GET environment variables 
const firebase = require("firebase-admin");
const serviceAccount = require("../config/pabs-63365-firebase-adminsdk-ruvd0-971389b7eb.json");
const StorageService = {};
const controllerName = "StorageService";

StorageService.uploadFile = async(file_name) => {
    const bucketName = 'pabs-63365.appspot.com'
    if (!firebase.apps.length) {
        firebase.initializeApp({
            credential: firebase.credential.cert(serviceAccount),
            storageBucket: bucketName,
        });
    }
    // const bucket = firebase.storage().bucket();
    const storageRef = firebase.storage().bucket(bucketName);

    // let file_name = "balance.pdf";
    let file_path = './balances/' + file_name;
    //let file_path = ENV.PATH + file_name;

    const uploadTask = storageRef.upload(file_path);
    return await uploadTask.then(upload => {
        // console.log('Archivo cargado...|', upload);
        console.log('Download URL|', 'https://firebasestorage.googleapis.com/v0/b/' + bucketName + '/o/' + file_name + "?alt=media&token=")
        return 'https://firebasestorage.googleapis.com/v0/b/' + bucketName + '/o/' + file_name + "?alt=media&token=";
        //return Promise.resolve("https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + encodeURIComponent(file.name) + "?alt=media&token=" + uuid);
    });

};
//console.log('|||');
//StorageService.uploadFile('salida.pdf');
// console.log(StorageService.uploadFile('salida.pdf'))
// StorageService.sendMessageTest();
module.exports = StorageService;