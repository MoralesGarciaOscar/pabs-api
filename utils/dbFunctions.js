const Sequelize = require("sequelize");
const DB = {};

DB.findOne = async(model, id) => {
    return model.findOne({ where: { id } }).then((data) => {
        return data;
    });
};
DB.findAll = async(model) => {
    return model
        .findAll({ attributes: { exclude: ["createdAt", "updatedAt"] } })
        .then((data) => {
            return data;
        });
};
DB.findOneCustomeField = async(model, customeField) => {
    return model.findOne({ where: customeField }).then((data) => {
        return data;
    });
};
DB.findAllCustomeField = async(model, customeField) => {
    return model.findAll({ where: customeField }).then((data) => {
        return data;
    });
};
// DB.findAllCustomeFilter = async(model, customeField) => {
//     return model.findAll({ where: customeField }).then(data => { return data });
// }
DB.findOneCustomQuery = async(model, query) => {
    return await model.sequelize
        .query(query, { type: Sequelize.QueryTypes.SELECT })
        .then((data) => {
            return data[0];
        });
};
DB.findAllCustomQuery = async(model, query) => {
    return await model.sequelize
        .query(query, {
            type: Sequelize.QueryTypes.SELECT,
            attributes: { exclude: ["createdAt", "updatedAt"] },
        })
        .then((data) => {
            return data;
        });
};

DB.update = async(model, paylod, id) => {
    return await model
        .update(paylod, { where: { id } })
        .then(([rowAffected, rowsUpdate, data]) => {
            return rowAffected;
        });
};
//Change the status, (logic deleted)
DB.delete = async(model, id) => {
    return await model
        .update({ status: 3 }, { where: { id } })
        .then(([rowAffected, rowsUpdate, data]) => {
            return rowAffected;
        });
};

DB.create = async(model, paylod) => {
    return await model.create(paylod).then((data) => {
        return data;
    });
};
DB.bulkCreate = async(model, paylod) => {
    return await model.bulkCreate(paylod).then((data) => {
        return data;
    });
};

module.exports = DB;