const encryptPassword = {};
const salt = "passwordKey";
const crypto = require('crypto');

encryptPassword.getEncyptPassword = (password) => {
    let hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update((password + ""));
    return hash.digest('hex');;
}


module.exports = encryptPassword;