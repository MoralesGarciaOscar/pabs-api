const template = (nombre, telefono, contrato, numero_contrato, colonia, domicilio, servicio, ataud, abono, dias_sin_abonar, atrasado, proximo_pago, frecuencia, fecha_contrato, costo, saldo, solicitud, localidad, monto_pago_actual, motivo, userPayments) => {
        return ` 
<head>
    <meta charset="UTF-8">
    <title>Estado de cuenta</title>
    <style>
    @import url("https://fonts.googleapis.com/css?family=DM+Sans:400,500,700&display=swap");
    html,
    body {
        margin: 5px;
        padding: 5px;
        width: 99%;
        background-color: white;
        font-family: 'DM Sans', sans-serif;
        font-size: 9px;
    }
    
    .header {
        width: 95%;
        display: flex;
        justify-content: center;
    }
    
    .container {
        width: 95%;
        display: flex;
        justify-content: space-between;
    }
    
    .right {
        display: flex;
        justify-content: right;
        flex-direction: column;
    }
    
    .center {
        display: flex;
        justify-content: center;
        flex-direction: column;
    }

    .left {
        display: flex;
        justify-content: left;
        flex-direction: column;
    }
    
    .saldos {
        font-size: 10px;
        padding-top: 5px;
    }
    
    label {
        font-size: 10px;
        padding-top: 5px;
    }
    table th{
        font-size: 10px;
    }
    table tr td{
        font-size: 8px;
    }
</style>
</head>

<body>

<div class="header">
<h1 align="center"> Programa de Apoyo de Beneficio Social</h1>
</div>
<div  align="center" class="header">
<strong>Planes Provisores de Colima S.A. de C.V.</strong>
</div>


    <div class="container">
        <div align="center"> ESTADO DE CUENTA </div>
        <div class="right">
        <table> 
        <tr> 
            <td> Contrato: ${contrato}  </td>
            <td> Fecha de contrato: ${fecha_contrato}  </td>
            <td> solicitud: ${solicitud}  </td>
            <td> Costo: $ ${costo} </td>
        </tr>
        <tr> 
            <td> Cliente: ${nombre}  </td>
            <td> Teléfono : ${fecha_contrato}  </td>
            <td> Saldo: ${saldo}  </td>
            <td> Colonia: ${colonia} </td>
        </tr>
        <tr> 
            <td> Localidad: ${localidad}  </td>
            <td> domicilio : ${domicilio}  </td> 
        </tr>
        <tr> 
            <td> Frecuencia de pago Actual: ${frecuencia}  </td>
            <td></td> <td></td>
            <td> Importe de Pago Actual : ${monto_pago_actual}  </td> 
        </tr>
        </table>
            
        </div>
        
    </div>
   
    <table>
        <th>Fecha Recibo </th> <th>Recibo </th> <th>Importe </th> <th>Cobrador </th> <th>Movimiento </th>
        ${ list=  userPayments.map(function(valor, i) {
            return `<tr>
                        <td>${valor['fecha_recibo']}</td> <td>${valor['recibo']}</td> <td> ${valor['importe']}</td> <td> ${valor['cobrador']}</td> <td> ${valor['movimiento']}</td>
                </tr> `
            })
        }
    </table>

    <table>   
    <tr> 
        <td> Dias de abonar: ${dias_sin_abonar | 0 } dia(s) </td>
        <td></td>  
        <td> Status : Activo  </td> 
    </tr>
    <tr> 
        <td> Monto atrasado: $ ${atrasado | 0  }  </td>
        <td></td>  
        <td> Motivo : ${motivo}  </td> 
    </tr>

    <tr>  
        <td></td>  
    </tr>
    <tr> 
        <td> Servicio :  ${servicio }  </td>
        <td></td>  
        <td> Ataud : ${ataud}  </td> 
    </tr>
        </table>
      

</body>
</html>
    `
}
module.exports = template;