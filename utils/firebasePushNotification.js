// require('./global'); //load global.js to add new functions to Global enviroment
const firebase = require("firebase-admin");
const serviceAccount = require("../config/pabs-63365-firebase-adminsdk-ruvd0-971389b7eb.json");
const NotificationService = {};
const controllerName = "NotificationService"

NotificationService.sendMessageByToken = (devicetoken, payload) => {
    if (!firebase.apps.length) {
        firebase.initializeApp({
            credential: firebase.credential.cert(serviceAccount),
            sender_id: '277370995284'
        });
    }
    const message = {
        data: {
            score: '850',
            time: '2:45'
        },
        token: devicetoken,
        notification: {
            body: payload.data.body,
            title: payload.data.title,
            image: payload.data.image, //'https://firebasestorage.googleapis.com/v0/b/pabs-63365.appspot.com/o/image%2Finsurgentes.jpg?alt=media&token=978c383b-909b-4f55-8d6c-7f15bcf3d873'
        }
    };
    console.log('payload|', payload);
    if (devicetoken && devicetoken != undefined && devicetoken != null) {
        log(controllerName + ' firebase params | devicetoken ' + devicetoken)
        log(controllerName, payload)
        return firebase.messaging().send(message)
            .then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
                return response;
            })
            .catch((error) => {
                console.log('Error sending message:', error);
                throw error;
            });
    } else {
        throw 'El token del dispositivo es requerido.';
    }
}


NotificationService.sendMessageMultipeToken = (devicetokens, payload) => {
    //references => https://firebase.google.com/docs/cloud-messaging/send-message#node.js_1
    if (!firebase.apps.length) {
        firebase.initializeApp({
            credential: firebase.credential.cert(serviceAccount),
            sender_id: '277370995284'
        });
    }
    const message = {
        data: {
            score: '850',
            time: '2:45'
        },
        tokens: devicetokens, // This registration token comes from the client FCM SDKs.
        notification: {
            body: 'Hola bienvenidos a PABS!',
            title: 'Bienvenido Daniel'
        }
    };
    if (devicetokens && devicetokens != null) {
        log(controllerName + ' firebase params | devicetokens ' + devicetokens)
        log(controllerName, payload)
        return firebase.messaging().sendMulticast(message)
            .then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
                return response;
            }).catch((error) => {
                console.log('Error sending message:', error);
                throw error;
            });
    } else {
        throw 'La lista de tokens es requerido.';
    }
}

// function sendMessage() {
//     //references => https://firebase.google.com/docs/cloud-messaging/send-message#node.js_1
//     if (!firebase.apps.length) {
//         firebase.initializeApp({
//             credential: firebase.credential.cert(serviceAccount),
//             sender_id: '277370995284'
//         });
//     }
//     // This registration token comes from the client FCM SDKs.
//     const registrationToken = 'fuqhzsliTeCBT3LXS5-kTO:APA91bFn-Ww4ZPuCANEUw09K_uQi8EKPBetAQ6JxXX1KSuvtbkwcb0jeoQsj8wdMEGiqVVMTHQG_BNXXVbGIXGbfOQODTB9JaV7Nf5-vfDBYSw6bXiekN2eD_DNXQrhWFv1u0J0gxmr5';

//     const message = {
//         data: {
//             score: '850',
//             time: '2:45'
//         },
//         token: registrationToken,
//         notification: {
//             body: 'Hola bienvenido a PABS!',
//             title: 'Bienvenido'
//         }
//     };

//     // Send a message to the device corresponding to the provided
//     // registration token.
//     firebase.messaging().send(message)
//         .then((response) => {
//             // Response is a message ID string.
//             console.log('Successfully sent message:', response);
//         })
//         .catch((error) => {
//             console.log('Error sending message:', error);
//         });
// }

// function sendMessageMultiple() {
//     //references => https://firebase.google.com/docs/cloud-messaging/send-message#node.js_1
//     if (!firebase.apps.length) {
//         firebase.initializeApp({
//             credential: firebase.credential.cert(serviceAccount),
//             sender_id: '277370995284'
//         });
//     }
//     // This registration token comes from the client FCM SDKs.
//     const registrationTokens = [
//         'fuqhzsliTeCBT3LXS5-kTO:APA91bFn-Ww4ZPuCANEUw09K_uQi8EKPBetAQ6JxXX1KSuvtbkwcb0jeoQsj8wdMEGiqVVMTHQG_BNXXVbGIXGbfOQODTB9JaV7Nf5-vfDBYSw6bXiekN2eD_DNXQrhWFv1u0J0gxmr5',
//         'fuqhzsliTeCBT3LXS5-kTO:APA91bFn-Ww4ZPuCANEUw09K_uQi8EKPBetAQ6JxXX1KSuvtbkwcb0jeoQsj8wdMEGiqVVMTHQG_BNXXVbGIXGbfOQODTB9JaV7Nf5-vfDBYSw6bXiekN2eD_DNXQrhWFv1u0J0gxmr5',
//     ];
//     const message = {
//         data: {
//             score: '850',
//             time: '2:45'
//         },
//         tokens: registrationTokens,
//         notification: {
//             body: 'Hola bienvenidos a PABS!',
//             title: 'Bienvenido Daniel'
//         }
//     };
//     firebase.messaging().sendMulticast(message)
//         .then((response) => {
//             console.log(response.successCount + ' messages were sent successfully');
//         });
// }
module.exports = NotificationService