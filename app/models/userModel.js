const Sequelize = require("sequelize");
const conexion = require("../../config/conexion/conexion");

const user = conexion.define("pabs_users", {
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true,
  },
  identificador: Sequelize.STRING,
  contract_id: Sequelize.INTEGER,
  name: Sequelize.STRING,
  firebase_token: Sequelize.STRING,
  password: Sequelize.STRING,
  email: Sequelize.STRING,
  phone: Sequelize.STRING,
  address: Sequelize.STRING,
  confirmed: Sequelize.BOOLEAN,
  reset: Sequelize.BOOLEAN,
  admin: Sequelize.BOOLEAN,
  status: {
    //status of the procces or activity
    type: Sequelize.INTEGER,
    defaultValue: 1,
  },
  // enable: { //true if the object is active, this field is has a false value when te user is deleted
  //     type: Sequelize.INTEGER,
  //     defaultValue: 1
  // },
  createdAt: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW,
  },
  updatedAt: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW,
  },
});

module.exports = user;
