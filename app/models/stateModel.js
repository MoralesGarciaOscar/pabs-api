const Sequelize = require("sequelize");
const conexion = require("../../config/conexion/conexion");

const state = conexion.define(
    "pabs_states", {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true,
        },
        name: Sequelize.STRING,
    }, {
        timestamps: false,
    }
);

module.exports = state;