const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion');

const notificactions = conexion.define('pabs_notificactions', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    id_contrato: Sequelize.INTEGER,
    title: Sequelize.STRING,
    description: Sequelize.STRING,
    firebase_token: Sequelize.STRING,
    status: { //status of the procces or activity
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    updatedAt: Sequelize.DATE
});


module.exports = notificactions