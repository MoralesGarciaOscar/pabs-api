const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion');

const faqs = conexion.define('pabs_faqs', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    external_url: Sequelize.STRING,
    status: { //status of the procces or activity
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    enable: { //true if the object is active, this field is has a false value when te user is deleted 
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
});


module.exports = faqs;