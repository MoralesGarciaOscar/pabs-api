const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion')

const banlance = conexion.define('pabs_client_balances', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    id_contrato: Sequelize.INTEGER,
    contrato: Sequelize.STRING,
    fecha_contrato: Sequelize.STRING,
    solicitud: Sequelize.STRING,
    cliente: Sequelize.STRING,
    telefono: Sequelize.STRING,
    domicilio: Sequelize.STRING,
    colonia: Sequelize.STRING,
    localidad: Sequelize.STRING,
    costo: Sequelize.STRING,
    saldo: Sequelize.STRING,
    fecha_recibo: Sequelize.STRING,
    recibo: Sequelize.STRING,
    importe: Sequelize.STRING,
    cobrador: Sequelize.STRING,
    forma_pago_actual: Sequelize.STRING,
    monto_pago_actual: Sequelize.STRING,
    estatus: Sequelize.STRING,
    motivo: Sequelize.STRING,
    movimiento: Sequelize.STRING,
    servicio: Sequelize.STRING,
    serie: Sequelize.STRING,
    ataud: Sequelize.STRING,
    // dias_sin_abonar: Sequelize.INTEGER,
    // status: { //status of the procces or activity
    //     type: Sequelize.INTEGER,
    //     defaultValue: 1
    // },
    // enable: { //true if the object is active, this field is has a false value when te user is deleted 
    //     type: Sequelize.INTEGER,
    //     defaultValue: 1
    // },
    // createdAt: {
    //     type: Sequelize.DATE,
    //     defaultValue: Sequelize.NOW,
    // },
    // updatedAt: {
    //     type: Sequelize.DATE,
    //     defaultValue: Sequelize.NOW
    // }
}, {
    timestamps: false
});


module.exports = banlance