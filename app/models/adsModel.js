const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion');

const ads = conexion.define('pabs_ads', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    image_url: Sequelize.STRING,
    external_url: Sequelize.STRING,
    status: { //status of the procces or activity
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    enable: { //true if the object is active, this field is has a false value when te user is deleted 
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
});


module.exports = ads