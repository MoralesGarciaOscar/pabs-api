const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion');

const place = conexion.define('pabs_places', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    state_id: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
    },
    city: Sequelize.STRING,
    address: Sequelize.STRING,
    phone: Sequelize.STRING,
    latitude: Sequelize.STRING,
    longitude: Sequelize.STRING,
    status: { //status of the procces or activity
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    enable: { //true if the object is active, this field is has a false value when te user is deleted 
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    updatedAt: Sequelize.DATE,
});


module.exports = place