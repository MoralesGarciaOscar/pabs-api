const Sequelize = require('sequelize');
const conexion = require('../../config/conexion/conexion');

const payment = conexion.define('pabs_payments', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    id_contrato: Sequelize.INTEGER,
    payment_method: Sequelize.STRING,
    amount: Sequelize.DOUBLE,
    paypal_track_id: Sequelize.STRING,
    status: { //status of the procces or activity
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    updatedAt: Sequelize.DATE
});


module.exports = payment