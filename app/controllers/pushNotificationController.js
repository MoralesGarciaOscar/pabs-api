const notificationsModel = require('../models/notificationModel'),
    //modelRequest = require('../Models/requestModel');
    utils = require("../../utils/encryptPassword"),
    Util = require("../../utils/util")
const pushNotificationController = {};
const controllerName = "PushNotificationController"
const admin = require("firebase-admin");
const firebase = require("../../utils/firebasePushNotification")

var serviceAccount = require("../../config/pabs-63365-firebase-adminsdk-ruvd0-971389b7eb.json");
const Sequelize = require('sequelize');


pushNotificationController.newMessage = (req, res) => {
    let { devicetoken } = req.body;
    if (!admin.apps.length) {
        //firebase.initializeApp({});
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            //databaseURL: "https://urdilab.firebaseio.com",
            sender_id: '632480439412'
        });
    }

    var token = "chhpGP9NS6Wln59M-zgYgu:APA91bFRWc4zkJjZt6WxsrumSafaVUQLOYyiKKM3q1HkndJ2YvEpnsT_07V57wXYYToLTVKhmPDBoE6hB7dPoWuDKUXftvCIC8_e15-4rst5DH7zr5e2xUgLQJJA8RmchQCUVrp9gIps"
    var payload = {
            data: {
                requestID: "100",
                app: "flebotomista",
                title: 'Tarifa confirmada',
                body: 'El usuario confirmó la Tarifa'
            }
        }
        //app: "flebotomista", "cliente"
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    }
    var message = {
        data: {
            score: '850',
            time: '2:45',
            sender_id: '348668667430',
        },
        notification: {
            title: 'Hello',
            body: 'Hello word, this is a Notification test'
        },
        token
    };

    /*
        admin.messaging().send(message).then((response) => {
            console.log('The message was sent successfully:', response)
            Util.message(res, response)
    
        }).catch((error) => {
            console.log('The message was not sent:', error)
            Util.errorMessage(res, error, Util.readMessage(controllerName, error))
        }) 
*/
    if (devicetoken && devicetoken != undefined && devicetoken != null) {
        var tokenRegistration = devicetoken //? devicetoken : 'cGvqQvHvSYCh0Ys18aS8Zf:APA91bH-MHkWXs8GdRhEyLSaP8hvOhGvPcV6jWarkWDCsh5gOQpaQ94SHlpBpbUfDuT4sCE8Hxc3fUqmSj2wfXRY_hI8xbuiwAljh9Xm9zILsgYl1zWPwAwovFZhn6ciukBujmaeDkn4'
        admin.messaging().sendToDevice(tokenRegistration, payload, options).then((response) => {
            console.log('The message was sent successfully:', response)
            Util.message(res, response)
        }).catch((error) => {
            console.log('The message was not sent:', error)
            Util.errorMessage(res, error, Util.readMessage(controllerName, error))
        })
    } else {
        Util.errorMessage(res, 'El token del dispositivo es requerido.', Util.readMessage(controllerName, 'Token no valido'))
    }

    /* adminModel.findAll().then(data => {
         if (data) { 
             Util.message(res, data)
         } else {
             Util.message(res, data, Util.notFoundMessage(controllerName))
         }
     }).catch(error => {
         Util.print(controllerName, Util.readMessage(controllerName, error))
         Util.errorMessage(res, error, Util.readMessage(controllerName, error))
     });
 */
}

pushNotificationController.sendMessagetoUser = async(req, res) => {
    let { devicetoken } = req.body;
    let payload = {
        data: {
            app: "pabs",
            title: 'Hola Daniel',
            body: 'Bienvenido'
        }
    }
    try {
        let fireBaseResponse = await firebase.sendMessageByToken(devicetoken, payload);
        send(res, fireBaseResponse, 'Mensajes enviados correctamente');
    } catch (error) {
        console.log('The message was not sent:', error)
    }

}


// pushNotificationController.sendMessageAll = async(req, res) => {
//     console.log('paso por aqui');
//     let { devicetoken } = req.body;
//     let payload = {
//         data: {
//             app: "pabs",
//             title: 'Hola Daniel',
//             body: 'Bienvenido'
//         }
//     }
//     try {
//         let fireBaseResponse = await firebase.sendMessageByToken(devicetoken, payload);
//         send(res, fireBaseResponse, 'Mensajes enviados correctamente');
//     } catch (error) {
//         console.log('The message was not sent:', error)
//     }

// }


pushNotificationController.sendMessageByRequestId = async(req, res) => {
    let { requestid } = req.params;
    var dataUser = req.decoded.dataUser
    let firebase_token = dataUser.firebase_token
    var payload = req.body;
    console.log('sendMessageByRequestId');
    payload.data.requestID = requestid
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    }
    try {

        let fireBaseResponse = await firebase.sendMessageByToken(firebase_token, payload, options)
        notificationsModel.create({ user_id: dataUser.id, request_id: requestid, title: payload.data.title, body: payload.data.body, app: payload.data.app, send: fireBaseResponse.successCount }).then(notification => {
            Util.print(controllerName, notification.dataValues)
        })
        Util.print(controllerName, fireBaseResponse)
        Util.message(res, fireBaseResponse)

    } catch (error) {
        Util.print(controllerName, error)
        Util.errorMessage(res, error, error + ',  Error to send the notification  | ' + controllerName)
    }

}




pushNotificationController.getAllMessage = (req, res) => {
    let { devicetoken } = req.body;
    if (!admin.apps.length) {
        //firebase.initializeApp({});
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://urdilab.firebaseio.com",
            sender_id: '348668667430'
        });
    }

    var token = "AAAAUS5FpiY:APA91bHM6mhWPsJGQc1mn2ubrbR8U6E22qapmQ6DT1-0G-KubtE5tfCRHqTPOyuw1iPeHr5NCkJE2mjL5qD8-F7uGo66ieJH2KHEr0VGVmG13lMGQBO2W_duoLcSj6qdN-j3QwA80Pyr"
    var payload = {
            data: {
                requestID: "100",
                app: "flebotomista",
                title: 'Tarifa confirmada',
                body: 'El usuario confirmó la Tarifa'
            }
        }
        //app: "flebotomista", "cliente"
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    }
    var message = {
        data: {
            score: '850',
            time: '2:45',
            sender_id: '348668667430',
        },
        notification: {
            title: 'Hello',
            body: 'Hello word, this is a Notification test'
        },
        token
    };

    /*
        admin.messaging().send(message).then((response) => {
            console.log('The message was sent successfully:', response)
            Util.message(res, response)
    
        }).catch((error) => {
            console.log('The message was not sent:', error)
            Util.errorMessage(res, error, Util.readMessage(controllerName, error))
        }) 
*/
    if (devicetoken && devicetoken != undefined && devicetoken != null) {
        var tokenRegistration = devicetoken //? devicetoken : 'cGvqQvHvSYCh0Ys18aS8Zf:APA91bH-MHkWXs8GdRhEyLSaP8hvOhGvPcV6jWarkWDCsh5gOQpaQ94SHlpBpbUfDuT4sCE8Hxc3fUqmSj2wfXRY_hI8xbuiwAljh9Xm9zILsgYl1zWPwAwovFZhn6ciukBujmaeDkn4'
        admin.messaging().sendToDevice(tokenRegistration, payload, options).then((response) => {
            console.log('The message was sent successfully:', response)
            Util.message(res, response)
        }).catch((error) => {
            console.log('The message was not sent:', error)
            Util.errorMessage(res, error, Util.readMessage(controllerName, error))
        })
    } else {
        Util.errorMessage(res, 'El token del dispositivo es requerido.', Util.readMessage(controllerName, error))
    }

    /* adminModel.findAll().then(data => {
         if (data) { 
             Util.message(res, data)
         } else {
             Util.message(res, data, Util.notFoundMessage(controllerName))
         }
     }).catch(error => {
         Util.print(controllerName, Util.readMessage(controllerName, error))
         Util.errorMessage(res, error, Util.readMessage(controllerName, error))
     });
 */
}





pushNotificationController.sendMessageByUserType = async(req, res) => {
    let { type } = req.params;
    var payload = req.body;
    var dataUser = req.decoded.dataUser
    console.log('type | ', type);
    // console.log(controllerName, dataUser);
    //var payload = {  data: {  requestID: id,  app: "cliente"  },  notification: {  title: 'Solicitud Confirmada',  body: 'El flebotomista ha confirmado tu solicitud.'  } }
    let user = await getUserInformation(type);
    console.log('USER FOUND | ', user);
    try {
        // console.log('firebase_token : ', firebase_token)
        user.forEach(async(user) => {
            if (user && user.firebase_token && user.firebase_token != null) {
                payload.data.requestID = user.requestID + '';
                // console.log('firebase_token | ', user.firebase_token);
                let fireBaseResponse = await firebase.sendMessageByToken(user.firebase_token, payload);
                await notificationsModel.create({ user_id: dataUser.id, request_id: user.requestID, title: payload.notification.title, body: payload.notification.body, app: payload.data.app, send: fireBaseResponse.successCount }).then(async notification => {
                        log('===============================================================================' +
                            controllerName + ' | firebase_token | ' + user.firebase_token + ' | successCount ' + fireBaseResponse.successCount +
                            '===============================================================================');
                        // log(controllerName, notification.dataValues)
                    })
                    // log(controllerName, fireBaseResponse)
            }
        });
        // console.log(controllerName, user);
        send(res, 'Mensajes enviados correctamente', 'Mensajes enviados correctamente al ' + type + '.')

    } catch (error) {
        log(controllerName, error)
        sendError(res, { data: "Error al enviar notificaciones al " + type + "." }, error)
    }

}


async function getUserInformation(type) {
    if (type == 'flebotomista') {
        // return await modelRequest.sequelize.query('select a.id as requestID, c.id , c.firebase_token from urdilab_requests a , urdilab_collectors b , urdilab_users c where a.collector_id =b.id and b.user_id=c.id  group by c.id ;', { type: Sequelize.QueryTypes.SELECT }).then((data) => {
        return await modelRequest.sequelize.query('select a.id as requestID, c.id , c.firebase_token from urdilab_requests a , urdilab_collectors b , urdilab_users c where a.collector_id =b.id and b.user_id=c.id and  c.firebase_token  is not null   group by c.id ;', { type: Sequelize.QueryTypes.SELECT }).then((data) => {
            return data
        })
    } else if (type == 'cliente') {
        return await modelRequest.sequelize.query('select a.id as requestID, c.id , c.firebase_token from urdilab_requests a , urdilab_clients b , urdilab_users c where a.client_id =b.id and b.user_id=c.id and  c.firebase_token is not null group by c.id;', { type: Sequelize.QueryTypes.SELECT }).then((data) => {
            return data
        })
    }

}

//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET   ALL NOTIFICATIONS                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+

pushNotificationController.getAllNotification = async(req, res) => {
    // let params = {}
    try {
        //var response = await safeExecution(" getAllNotification ", null, null, _getNotifications)
        send(res, await _getNotifications())
            //send(res, response)

    } catch (error) {
        log(controllerName, error)
        log(error.message)
        sendError(res, { data: "Error al obtener las notificaciones ." }, error)
    }
}

async function _getNotifications() {

    return await notificationsModel.sequelize.query('select a.id, a.app, a.title, a.body,a.send , b.username ,  a.createdAt as date  from urdilab_notifications a,  urdilab_users b where a.user_id=b.id order by a.id desc ;', { type: Sequelize.QueryTypes.SELECT }).then((data) => {
        return data
    })

}

pushNotificationController.getNotificationById = async(req, res) => {
    let { id } = req.params;
    log(id)
    try {
        //var response = await safeExecution(" getAllNotification ", null, null, _getNotifications)
        send(res, await _getNotificationsById(id))
            //send(res, response)

    } catch (error) {
        log(controllerName, error)
        log(error.message)
        sendError(res, { data: "Error al obtener las notificaciones ." }, error)
    }
}

async function _getNotificationsById(id) {

    return await notificationsModel.sequelize.query('select a.id, a.app, a.title, a.body,a.send , b.username ,  a.createdAt as date  from urdilab_notifications a,  urdilab_users b where a.user_id=b.id and  a.id = ' + id + ';', { type: Sequelize.QueryTypes.SELECT }).then((data) => {
        return data[0]
    })

}
/**
 * Imagenes con su descripcion para el carete
 * Direción, telefono, latitud y longitud 
 * Lista preguntas frecuentes
 * Crear 2 cuentas 1 para el playstore de google y otra para el appstore de Apple
 * 
 * 
 * Agregar imagenes en l push notificacions
 */
module.exports = pushNotificationController;