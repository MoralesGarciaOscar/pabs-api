const placeModel = require("../models/placeModel"),
    stateModel = require("../models/stateModel"),
    placeController = {},
    DB = require("../../utils/dbFunctions"),
    access_users = require('../../middleware/access_users'),
    encrypt = require("../../utils/encryptPassword"),
    Util = require("../../utils/util");
const controllerName = "placeController";
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW PLACE (BRANCH)                                                    |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.create = async(req, res) => {
    let paylod = req.body;
    try {
        const data = await DB.create(placeModel, paylod);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  user BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.read = async(req, res) => {
    try {
        const { id } = req.params;
        if (Util.integerIDValidation(res, id, controllerName)) {
            var data = await DB.findOne(placeModel, id);
            console.log('Data|', data);

            Util.dataValidation(res, data, controllerName);
        }
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF user CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.readAll = async(req, res) => {
    try {
        const { state_id } = req.query;
        let filter = (state_id && state_id !== null) ? { status: 1, state_id } : { status: 1 };
        const data = await DB.findAllCustomeField(placeModel, filter);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};


//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET   ALL USERS                                                              |
//|                                                                             |
//+-----------------------------------------------------------------------------+
// placeController.getUsers = (req, res) => {
//     users.findAll({ attributes: ['id', 'email', 'image', 'status', 'enable'] }).then(data => {
//         dataValidation(res, data, controllerName);
//     }).catch(error => {
//         log(controllerName, createMessage(controllerName, error));
//         sendError(res, error, createMessage(controllerName, error));
//     });
// }
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   user  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.update = async(req, res) => {
    const { id } = req.params;
    const paylod = req.body;
    try {
        var data = await DB.update(placeModel, paylod, id);
        Util.updateValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   user  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        var data = await DB.delete(placeModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};



//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF STATES FROM MEXICO                                               |
//|                                                                             |
//+-----------------------------------------------------------------------------+
placeController.readAllStates = async(req, res) => {
    try {
        const data = await DB.findAll(stateModel);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
module.exports = placeController;