const faqsModel = require("../models/faqsModel"),
    faqsController = {},
    DB = require("../../utils/dbFunctions"),
    Util = require("../../utils/util");
const controllerName = "faqsController";
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW FAQs                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.create = async(req, res) => {
    let paylod = req.body;
    try {
        const data = await DB.create(faqsModel, paylod);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  pabs BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.read = async(req, res) => {
    try {
        const { id } = req.params;
        if (Util.integerIDValidation(res, id, controllerName)) {
            const data = await DB.findOne(InventoryModel, id);
            Util.dataValidation(res, data, controllerName);
        }
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.readAll = async(req, res) => {
    try {
        const data = await DB.findAll(faqsModel);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.readAllByCustomQuery = async(req, res) => {
    try {
        let query = `select * from pabs_users`;
        const data = await DB.findAllCustomQuery(faqsModel, query);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   pabs  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.update = async(req, res) => {
    const { id } = req.params;
    const paylod = req.body;
    try {
        const data = await DB.update(faqsModel, paylod, id);
        Util.updateValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   pabs  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
faqsController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        const data = await DB.delete(faqsModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
module.exports = faqsController;