const paymentModel = require('../models/paymentModel'),
    banlanceModel = require('../models/balanceModel'),
    paymentController = {},
    DB = require('../../utils/dbFunctions'),
    Util = require('../../utils/util');
const controllerName = 'paymentController';
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW banlance                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
paymentController.create = async(req, res) => {
        const payload = req.body;
        try {
            const payment = await DB.create(paymentModel, payload);
            let { id_contrato } = payload;
            const clientData = await DB.findOneCustomeField(banlanceModel, { id_contrato });
            let userBalance = clientData.dataValues;
            userBalance.monto_pago_actual = payload.amount;
            userBalance.importe = payload.amount;
            userBalance.id = 0;
            console.log('userBalance|', userBalance)
            await DB.create(banlanceModel, userBalance);
            Util.dataValidation(res, payment, controllerName);
        } catch (error) {
            log(controllerName, Util.readMessage(controllerName, error))
            sendError(res, error, Util.readMessage(controllerName, error))
        }
    }
    //+-----------------------------------------------------------------------------+
    //|                                                                             |
    //|                                                                             |
    //|FIND  banlance BY ID                                                          |
    //|                                                                             |
    //+-----------------------------------------------------------------------------+
paymentController.read = async(req, res) => {
        try {
            const contract_id = req.params.id;
            //const contract_id = (req.decoded && req.decoded.dataUser)?req.decoded.dataUser.contract_id:0;
            // console.log('id |', id);
            if (Util.integerIDValidation(res, contract_id, controllerName)) {
                const data = await DB.findAllCustomeField(paymentModel, { "id_contrato": contract_id });
                // const data = await DB.findOne(paymentModel, contract_id)
                Util.dataValidation(res, data, controllerName)
            }
        } catch (error) {
            log(controllerName, Util.readMessage(controllerName, error))
            sendError(res, error, Util.readMessage(controllerName, error))
        }
    }
    //+-----------------------------------------------------------------------------+
    //|                                                                             |
    //|                                                                             |
    //|GET LIST OF banlance CUSTOM QUERY                                            |
    //|                                                                             |
    //+-----------------------------------------------------------------------------+
paymentController.readAll = async(req, res) => {
        try {
            var data = await DB.findAll(paymentModel)
            Util.dataValidation(res, data, controllerName)
        } catch (error) {
            log(controllerName, Util.readMessage(controllerName, error))
            sendError(res, error, Util.readMessage(controllerName, error))
        }
    }
    //+-----------------------------------------------------------------------------+
    //|                                                                             |
    //|                                                                             |
    //|UPDATE   banlance  BY ID                                                       |
    //|                                                                             |
    //+-----------------------------------------------------------------------------+
paymentController.update = async(req, res) => {
        const { id } = req.params;
        const paylod = req.body;
        try {
            var data = await DB.update(paymentModel, paylod, id);
            Util.updateValidation(res, data, controllerName);
        } catch (error) {
            log(controllerName, Util.readMessage(controllerName, error))
            sendError(res, error, Util.readMessage(controllerName, error))
        }
    }
    //+-----------------------------------------------------------------------------+
    //|                                                                             |
    //|                                                                             |
    //|DELETE   banlance  BY ID                                                      |
    //|                                                                             |
    //+-----------------------------------------------------------------------------+
paymentController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        var data = await DB.delete(paymentModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error))
        sendError(res, error, Util.readMessage(controllerName, error))
    }
}
module.exports = paymentController;