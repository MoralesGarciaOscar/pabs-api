const userModel = require("../models/userModel"),
  userController = {},
  DB = require("../../utils/dbFunctions"),
  access_users = require("../../middleware/access_users"),
  encrypt = require("../../utils/encryptPassword"),
  Util = require("../../utils/util");
const controllerName = "userController";
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW user                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.login = async (req, res) => {
  try {
    let { contract_id, password, firebase_token } = req.body;
    log(controllerName, req.body);
    //logJSON(req.body)
    password = encrypt.getEncyptPassword(password);
    await validateUser(contract_id, password, firebase_token, (isUser) => {
      if (isUser.value) {
        send(
          res,
          access_users.getToken({
            name: isUser.data.name,
            email: isUser.data.email,
            user_id: isUser.data.id,
            first_time: isUser.data.confirmed,
            contract_id: isUser.data.contract_id,
          })
        );
      } else {
        sendError(res, "No hay usuarios registrados con estas credenciales.");
      }
    }).catch((error) => {
      log(controllerName, Util.readMessage(controllerName, error));
      sendError(res, error, Util.readMessage(controllerName, error));
    });
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|VALIDATE IF USER EXIST                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+

const validateUser = async (contract_id, password, firebase_token, func) => {
  const userFound = await findUser(contract_id, password);
  if (userFound) {
    await updateUser(userFound.contract_id, firebase_token);
    return func({ value: true, data: await findUser(contract_id, password) });
  } else return func({ value: false, data: null });
};

//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE FIREBASE TOKEN AT  USER                                               |
//|                                                                             |
//+-----------------------------------------------------------------------------+
async function updateUser(contract_id, firebase_token) {
  userModel
    .update({ firebase_token: firebase_token }, { where: { contract_id } })
    .then((rowAffected) => {
      if (rowAffected > 0)
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " Firebase token  User updated successfully."
        );
      else
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " Firebase token  User updated Falied."
        );
    });
}

//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND USER                                                                    |
//|                                                                             |
//+-----------------------------------------------------------------------------+
async function findUser(contract_id, password) {
  return userModel
    .findOne({
      where: { contract_id, password, status: 1 },
      attributes: ["id", "contract_id", "name", "email", "confirmed"],
    })
    .then((user) => {
      return user;
    });
}

//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW user                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.create = async (req, res) => {
  let paylod = req.body;
  try {
    paylod.password = encrypt.getEncyptPassword(paylod.password);
    const data = await DB.create(userModel, paylod);
    Util.dataValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  user BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.read = async (req, res) => {
  try {
    const { id } = req.params;
    if (Util.integerIDValidation(res, id, controllerName)) {
      const data = await DB.findOne(userModel, id);
      Util.dataValidation(res, data, controllerName);
    }
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF user CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.readAll = async (req, res) => {
  try {
    const data = await DB.findAll(userModel);
    Util.dataValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF Active user CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.readAllActive = async (req, res) => {
  try {
    const query = `select id, name, firebase_token from pabs_users where firebase_token is not null `;
    const data = await DB.findAllCustomQuery(userModel, query);
    Util.dataValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET   ALL USERS                                                              |
//|                                                                             |
//+-----------------------------------------------------------------------------+
// userController.getUsers = (req, res) => {
//     users.findAll({ attributes: ['id', 'email', 'image', 'status', 'enable'] }).then(data => {
//         dataValidation(res, data, controllerName);
//     }).catch(error => {
//         log(controllerName, createMessage(controllerName, error));
//         sendError(res, error, createMessage(controllerName, error));
//     });
// }
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   user  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
userController.update = async (req, res) => {
  const { id } = req.params;
  const paylod = req.body;
  try {
    paylod.password = encrypt.getEncyptPassword(paylod.password);
    log(controllerName, paylod);
    const data = await DB.update(userModel, paylod, id);
    Util.updateValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|DELETE   user  BY ID                                                      |
//+-----------------------------------------------------------------------------+
userController.delete = async (req, res) => {
  const { id } = req.params;
  try {
    const data = await DB.delete(userModel, id);
    Util.deleteValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|LOGOUT  USERS                                                                |
//+-----------------------------------------------------------------------------+
userController.logOut = (req, res) => {
  access_users.distroyToken(req);
  send(res, "the session was closed successfully");
};

//+-----------------------------------------------------------------------------+
//| USER FIRTS TIME LOGGIN CONFIRMED                                            |
//+-----------------------------------------------------------------------------+
// userController.loginConfirm = async(req, res) => {
//     try {
//         const { contract_id } = req.params;
//         const data = await confirmLoginUser(contract_id);
//         Util.updateValidation(res, data, controllerName);
//     } catch (error) {
//         log(controllerName, Util.readMessage(controllerName, error));
//         sendError(res, error, Util.readMessage(controllerName, error));
//     }
// };
// async function confirmLoginUser(contract_id) {
//     return userModel.update({ confirmed: true, reset: false }, { where: { contract_id } }).then(rowAffected => {
//         if (rowAffected > 0) log(controllerName, "rowAffected | " + rowAffected + " |  User Log in confirmed successfully.");
//         else log(controllerName, "rowAffected | " + rowAffected + " | User Log in confirmed Falied.");
//         return rowAffected;
//     })
// }

//+-----------------------------------------------------------------------------+
//|RESET USER PASSWORD                                                          |
//+-----------------------------------------------------------------------------+
userController.passwordChange = async (req, res) => {
  try {
    let { contract_id, password } = req.body;
    const data = await userPasswordChange(contract_id, password);
    Util.updateValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
async function userPasswordChange(contract_id, password) {
  password = encrypt.getEncyptPassword(password);
  return userModel
    .update(
      { confirmed: true, reset: false, password },
      { where: { contract_id } }
    )
    .then((rowAffected) => {
      if (rowAffected > 0)
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " | Password User reset successfully."
        );
      else
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " | Password User reset updated Falied."
        );
      return rowAffected;
    });
}

//+-----------------------------------------------------------------------------+
//|RESET USER PASSWORD                                                          |
//+-----------------------------------------------------------------------------+
userController.passwordRecovery = async (req, res) => {
  try {
    const { contract_id } = req.params;
    const data = await resetPasswordUser(contract_id);
    Util.updateValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
async function resetPasswordUser(contract_id) {
  let password = encrypt.getEncyptPassword(contract_id);
  return userModel
    .update(
      { confirmed: false, reset: true, password },
      { where: { contract_id } }
    )
    .then((rowAffected) => {
      if (rowAffected > 0)
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " | Password User reset successfully."
        );
      else
        log(
          controllerName,
          "rowAffected | " +
            rowAffected +
            " | Password User reset updated Falied."
        );
      return rowAffected;
    });
}

module.exports = userController;
