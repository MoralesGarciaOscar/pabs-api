const adsModel = require("../models/adsModel"),
    adsController = {},
    DB = require("../../utils/dbFunctions"),
    Util = require("../../utils/util");
const controllerName = "adsController";
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW pabs                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.create = async(req, res) => {
    let paylod = req.body;
    try {
        const data = await DB.create(adsModel, paylod);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  pabs BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.read = async(req, res) => {
    try {
        const { id } = req.params;
        if (Util.integerIDValidation(res, id, controllerName)) {
            const data = await DB.findOne(InventoryModel, id);
            Util.dataValidation(res, data, controllerName);
        }
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.readAll = async(req, res) => {
    try {
        const data = await DB.findAll(adsModel);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.readAllByCustomQuery = async(req, res) => {
    try {
        let query = `select * from pabs_users`;
        const data = await DB.findAllCustomQuery(adsModel, query);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   pabs  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.update = async(req, res) => {
    const { id } = req.params;
    const paylod = req.body;
    try {
        const data = await DB.update(adsModel, paylod, id);
        Util.updateValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   pabs  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
adsController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        const data = await DB.delete(adsModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
module.exports = adsController;