const pabsModel = require("../models/pabsModel"),
    pabsController = {},
    DB = require("../../utils/dbFunctions"),
    Util = require("../../utils/util");
const controllerName = "pabsController";
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW pabs                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.create = async(req, res) => {
    let paylod = req.body;
    try {
        var data = await DB.create(pabsModel, paylod);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  pabs BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.read = async(req, res) => {
    try {
        const { id } = req.params;
        if (Util.integerIDValidation(res, id, controllerName)) {
            var data = await DB.findOne(InventoryModel, id);
            Util.dataValidation(res, data, controllerName);
        }
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.readAll = async(req, res) => {
    try {
        var data = await DB.findAll(pabsModel);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF pabs CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.readAllByCustomQuery = async(req, res) => {
    try {
        let query = `select * from pabs_users`;
        var data = await DB.findAllCustomQuery(pabsModel, query);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   pabs  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.update = async(req, res) => {
    const { id } = req.params;
    const paylod = req.body;
    try {
        var data = await DB.update(pabsModel, paylod, id);
        Util.updateValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   pabs  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
pabsController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        var data = await DB.delete(pabsModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        log(controllerName, Util.readMessage(controllerName, error));
        sendError(res, error, Util.readMessage(controllerName, error));
    }
};
module.exports = pabsController;