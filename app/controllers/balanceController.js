const banlanceModel = require("../models/balanceModel"),
    storage = require("../../utils/firebaseStorage"),
    banlanceController = {},
    pdf = require("html-pdf"),
    DB = require("../../utils/dbFunctions"),
    Util = require("../../utils/util"),
    balanceTemplate = require("../../utils/balance.template"),
    controllerName = "banlanceController";
const options = {
    format: "A4",
    header: {
        height: "60px",
    },
    footer: {
        height: "22mm",
    },
    base: "./balances/",
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW banlance                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.create = async(req, res) => {
    let paylod = req.body;
    try {
        const data = await DB.create(banlanceModel, paylod);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  banlance BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.read = async(req, res) => {
    try {
        const id_contrato = req.params.id;
        if (Util.integerIDValidation(res, id_contrato, controllerName)) {

            let query = `SELECT sum(saldo) as saldo,id_contrato , contrato, monto_pago_actual as abono, fecha_contrato, solicitud, cliente, telefono,
            domicilio, colonia,localidad, forma_pago_actual, monto_pago_actual, estatus ,servicio,serie,ataud,
            'Semanal'  as frecuencia, costo, saldo , (SELECT fecha_recibo FROM pabs_client.pabs_client_balances where id_contrato=10540 order by fecha_recibo desc limit 1 ) as ultimo_abono
         FROM pabs_client.pabs_client_balances where id_contrato =${id_contrato}    group by id_contrato `;
            const userBalance = await DB.findOneCustomQuery(banlanceModel, query);
            console.log(userBalance);
            userBalance.ultimo_abono = Util.DDMMYYYYDateFormat(userBalance.ultimo_abono);
            userBalance.proximo_pago = Util.DDMMYYYYDateFormat(new Date());

            // const data = await DB.findAllCustomeField(banlanceModel, { id_contrato });
            // data[0].dataValues.dias_sin_abonar = 5;
            // data[0].dataValues.monto_atrasado = 500;
            // data[0].dataValues.proximo_pago = new Date().toUTCString();
            // data[0].dataValues.pago = 1000;
            // console.log('data|', data[0].dataValues);
            // let userBalance = data[0];
            // P4bs.2021-API
            Util.dataValidation(res, userBalance, controllerName);
        }
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  banlance BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.createPDF = async(req, res) => {
    try {
        const id_contrato = req.params.id;
        if (Util.integerIDValidation(res, id_contrato, controllerName)) {
            let query = `SELECT sum(saldo) as saldo,id_contrato , contrato, motivo, monto_pago_actual as abono, fecha_contrato, solicitud, cliente, telefono,
            domicilio, colonia,localidad, forma_pago_actual, monto_pago_actual, estatus ,servicio,serie,ataud,
            'Semanal'  as frecuencia, costo, saldo , (SELECT fecha_recibo FROM pabs_client.pabs_client_balances where id_contrato=10540 order by fecha_recibo desc limit 1 ) as ultimo_abono
         FROM pabs_client.pabs_client_balances where id_contrato =${id_contrato}    group by id_contrato `;

            let queryPaymentList = `SELECT fecha_recibo , recibo, importe, cobrador, movimiento FROM pabs_client.pabs_client_balances where id_contrato =${id_contrato}   order by fecha_recibo `;

            const userBalance = await DB.findOneCustomQuery(banlanceModel, query);
            const userPayments = await DB.findAllCustomQuery(banlanceModel, queryPaymentList);
            console.log(userBalance);
            // console.log('userPayments|', userPayments);
            userBalance.proximo_pago = Util.DDMMYYYYDateFormat(new Date());
            //(nombre, telefono, contrato, numero_contrato, colonia, domicilio, servicio, ataud, abono, dias_sin_abonar, atrasado, proximo_pago, frecuencia, fecha_contrato, costo, saldo)
            const contenido = balanceTemplate(
                userBalance.cliente,
                userBalance.telefono,
                userBalance.contrato,
                userBalance.id_contrato,
                userBalance.colonia,
                userBalance.domicilio,
                userBalance.servicio,
                userBalance.ataud,
                userBalance.abono,
                userBalance.dias_sin_abonar,
                userBalance.atrasado,
                userBalance.proximo_pago,
                userBalance.frecuencia,
                userBalance.fecha_contrato,
                userBalance.costo,
                userBalance.saldo,
                userBalance.solicitud,
                userBalance.localidad,
                userBalance.monto_pago_actual,
                userBalance.motivo,
                userPayments
            );
            // ` <h1>Estado de cuenta</h1>
            //     <h2> ${userBalance.cliente}</h2>
            //     <h3> ${userBalance.id_contrato} - ${userBalance.contrato}</h4>
            //     <h3> 🚀 ${userBalance.telefono}</h4>
            //                 <p>${userBalance.domicilio} - ${userBalance.colonia} - ${userBalance.localidad}</p>`;
            const file_name =
                userBalance.id_contrato + "-" + userBalance.contrato + ".pdf";
            pdf
                .create(contenido, options)
                .toFile(options.base + file_name, async function(err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(result);
                        //let url = await storage.uploadFile(file_name);
                        let url = 'https://firebasestorage.googleapis.com/v0/b/marketplace-31cf0.appspot.com/o/pabs%2FESTADO%20DE%20CUENTA%20D15574.pdf?alt=media&token=69e7c450-9e2f-413d-ae2f-d6dfcde1075d'
                        let month =
                            getMonth(new Date().getMonth()) + " " + new Date().getFullYear();
                        // send(respuesta, data, message, codeResponse);
                        Util.dataValidation(res, { month, url }, controllerName);
                    }
                });
            // P4bs.2021-API
        }
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};

function getMonth(month) {
    let mes = "Enero";
    switch (month) {
        case 0:
            mes = "Enero";
            break;
        case 1:
            mes = "Febrero";
            break;
        case 2:
            mes = "Marzo";
            break;
        case 3:
            mes = "Abril";
            break;
        case 4:
            mes = "Mayo";
            break;
        case 5:
            mes = "Junio";
            break;
        case 6:
            mes = "Julio";
            break;
        case 7:
            mes = "Agosto";
            break;
        case 8:
            mes = "Septiembre";
            break;
        case 9:
            mes = "Octubre";
            break;
        case 10:
            mes = "Noviembre";
            break;
        default:
            mes = "Diciembre";
            break;
    }
    return mes;
}
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF banlance CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.readAll = async(req, res) => {
    try {
        var data = await DB.findAll(banlanceModel);
        Util.dataValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   banlance  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.update = async(req, res) => {
    const { id } = req.params;
    const paylod = req.body;
    try {
        var data = await DB.update(banlanceModel, paylod, id);
        Util.updateValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   banlance  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
banlanceController.delete = async(req, res) => {
    const { id } = req.params;
    try {
        var data = await DB.delete(banlanceModel, id);
        Util.deleteValidation(res, data, controllerName);
    } catch (error) {
        sendError(res, error.message, Util.readMessage(controllerName, error));
    }
};
module.exports = banlanceController;