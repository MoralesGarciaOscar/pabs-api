const notificacionModel = require("../models/notificationModel"),
  notificationController = {},
  DB = require("../../utils/dbFunctions"),
  Util = require("../../utils/util");
const controllerName = "notificationController";
const firebase = require("../../utils/firebasePushNotification");
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|CREATE NEW banlance                                                                |
//|                                                                             |
//+-----------------------------------------------------------------------------+
notificationController.create = async (req, res) => {
  let body = req.body;
  let payload = {
    data: {
      app: "pabs",
      title: body.title,
      body: body.description,
      image: body.image,
      firebase_token: body.firebase_token,
    },
  };
  const devicetoken = body.firebase_token;
  try {
    let data = await DB.create(notificacionModel, body);
    data.fireBaseResponse = await firebase.sendMessageByToken(
      devicetoken,
      payload
    );
    send(res, data, "Mensajes enviados correctamente");
    // Util.dataValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|FIND  banlance BY ID                                                          |
//|                                                                             |
//+-----------------------------------------------------------------------------+
notificationController.read = async (req, res) => {
  try {
    const id_contrato = req.params.id;
    //const contract_id = (req.decoded && req.decoded.dataUser)?req.decoded.dataUser.contract_id:0;
    // console.log('id |', id);
    if (Util.integerIDValidation(res, id_contrato, controllerName)) {
      const data = await DB.findAllCustomeField(notificacionModel, {
        id_contrato,
        status: 1,
      });
      // const data = await DB.findOne(notificacionModel, contract_id)
      Util.dataValidation(res, data, controllerName);
    }
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|GET LIST OF banlance CUSTOM QUERY                                            |
//|                                                                             |
//+-----------------------------------------------------------------------------+
notificationController.readAll = async (req, res) => {
  try {
    let data = await DB.findAll(notificacionModel);
    Util.dataValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|UPDATE   banlance  BY ID                                                       |
//|                                                                             |
//+-----------------------------------------------------------------------------+
notificationController.update = async (req, res) => {
  const { id } = req.params;
  const paylod = req.body;
  try {
    let data = await DB.update(notificacionModel, paylod, id);
    Util.updateValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
//+-----------------------------------------------------------------------------+
//|                                                                             |
//|                                                                             |
//|DELETE   banlance  BY ID                                                      |
//|                                                                             |
//+-----------------------------------------------------------------------------+
notificationController.delete = async (req, res) => {
  const { id } = req.params;
  try {
    let data = await DB.delete(notificacionModel, id);
    Util.deleteValidation(res, data, controllerName);
  } catch (error) {
    log(controllerName, Util.readMessage(controllerName, error));
    sendError(res, error, Util.readMessage(controllerName, error));
  }
};
module.exports = notificationController;
