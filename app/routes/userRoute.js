var express = require("express"),
  router = express.Router(),
  userController = require("../controllers/userController"),
  access_users = require("../../middleware/access_users"),
  trafficUser = require("../../middleware/trafficUser");

router.post("/login", trafficUser.save, userController.login);
router.post(
  "/users",
  [trafficUser.save, access_users.authentication],
  userController.create
);
router.get(
  "/users",
  [trafficUser.save, access_users.authentication],
  userController.readAll
);
router.get(
  "/users/active",
  [trafficUser.save, access_users.authentication],
  userController.readAllActive
);
router.get(
  "/users/:id",
  [trafficUser.save, access_users.authentication],
  userController.read
);
router.post(
  "/users/passwordchange",
  [trafficUser.save, access_users.authentication],
  userController.passwordChange
);
router.get(
  "/users/passwordrecovery/:contract_id",
  [trafficUser.save],
  userController.passwordRecovery
);
// router.get('/users/confirmed/:contract_id', [trafficUser.save, access_users.authentication], userController.loginConfirm);
router.put(
  "/users/:id",
  [trafficUser.save, access_users.authentication],
  userController.update
);
router.delete(
  "/users/:id",
  [trafficUser.save, access_users.authentication],
  userController.delete
);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;
