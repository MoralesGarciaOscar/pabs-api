/**
 const express = require('express'),
    router = express.Router(),
    PushNotificationController = require('../controllers/pushNotificationController'),
    access_users = require('../../middleware/access_users');

router.get('/notification', access_users.authentication, PushNotificationController.newMessage);
// router.post('/notification', access_users.authentication, PushNotificationController.sendMessageAll);
router.post('/notification/:requestid', access_users.authentication, PushNotificationController.sendMessageByRequestId);
router.get('/notification', access_users.authentication, PushNotificationController.getAllMessage);
router.post('/notifications/:type', access_users.authentication, PushNotificationController.sendMessageByUserType);
router.get('/notifications', access_users.authentication, PushNotificationController.getAllNotification);
router.get('/notifications/:id', access_users.authentication, PushNotificationController.getNotificationById);


module.exports = router;

*/