var express = require('express'),
    router = express.Router(),
    palceController = require('../controllers/placeController'),
    access_users = require('../../middleware/access_users'),
    trafficUser = require('../../middleware/trafficUser');

router.post('/places', [trafficUser.save, access_users.authentication], palceController.create);
router.get('/places', [trafficUser.save, access_users.authentication], palceController.readAll);
router.get('/states', [trafficUser.save, access_users.authentication], palceController.readAllStates);
router.get('/places/:id', [trafficUser.save, access_users.authentication], palceController.read);
router.put('/places/:id', [trafficUser.save, access_users.authentication], palceController.update);
router.delete('/users/:id', [trafficUser.save, access_users.authentication], palceController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;