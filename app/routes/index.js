const express = require('express');
const app = express(),
    path = require('path');

const user = require('./userRoute');
const balance = require('./balanceRoute');
const config = require('./configRoute');
const pabs = require('./pabsRoute');
const place = require('./placeRoute');
const ads = require('./adsRoute');
const faq = require('./faqsRoute');
const payment = require('./paymentRoute');
const notification = require('./notificationRoute');
// const push = require('./pushNotificationRoutes');
app.get('/v1/swagger', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../swagger/swagger.yaml'));
})

app.use('/v1', user); //ALL USER  ENDPOINT
app.use('/v1', balance); //ALL BALANCE  ENDPOINT
app.use('/v1', pabs); //ALL PABS  ENDPOINT
app.use('/v1', config); //ALL CONFIG  ENDPOINT
app.use('/v1', place); //ALL PLACE  ENDPOINT
app.use('/v1', ads); //ALL ADS  ENDPOINT
app.use('/v1', faq); //ALL FAQs  ENDPOINT
app.use('/v1', payment); //ALL PAYMENT  ENDPOINT
app.use('/v1', notification); //ALL NOTIFICATION  ENDPOINT
// app.use('/v1', push); //ALL PUSH NOTIFICATION  ENDPOINT

module.exports = app;