var express = require('express'),
    router = express.Router(),
    paymentController = require('../controllers/paymentController'),
    access_users = require('../../middleware/access_users'),
    trafficUser = require('../../middleware/trafficUser')


router.post('/payments', [trafficUser.save, access_users.authentication], paymentController.create);
router.get('/payments', [trafficUser.save, access_users.authentication], paymentController.readAll);
router.get('/payments/:id', [trafficUser.save, access_users.authentication], paymentController.read)
router.put('/payments/:id', [trafficUser.save, access_users.authentication], paymentController.update);
router.delete('/payments/:id', [trafficUser.save, access_users.authentication], paymentController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;