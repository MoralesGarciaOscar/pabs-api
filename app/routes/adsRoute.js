var express = require('express'),
    router = express.Router(),
    adsController = require('../controllers/adsController'),
    access_users = require('../../middleware/access_users');


router.post('/ads', access_users.authentication, adsController.create);
router.get('/ads', access_users.authentication, adsController.readAll);
router.get('/ads/:id', access_users.authentication, adsController.readAllByCustomQuery);
router.put('/ads/:id', access_users.authentication, adsController.update);
router.delete('/ads/:id', access_users.authentication, adsController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;