var express = require('express'),
    router = express.Router(),
    pabsController = require('../controllers/pabsController'),
    access_users = require('../../middleware/access_users');


router.post('/pabss', access_users.authentication, pabsController.create);
//router.get('/pabss', access_users.authentication, pabsController.read );
router.get('/pabss', access_users.authentication, pabsController.readAllByCustomQuery);
router.get('/pabss/:id', access_users.authentication, pabsController.readAll);
router.put('/pabss/:id', access_users.authentication, pabsController.update);
router.delete('/pabss/:id', access_users.authentication, pabsController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;