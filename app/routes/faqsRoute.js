const express = require('express'),
    router = express.Router(),
    faqsController = require('../controllers/faqsController'),
    access_users = require('../../middleware/access_users');


router.post('/faqs', access_users.authentication, faqsController.create);
router.get('/faqs', access_users.authentication, faqsController.readAll);
router.get('/faqs/:id', access_users.authentication, faqsController.readAllByCustomQuery);
router.put('/faqs/:id', access_users.authentication, faqsController.update);
router.delete('/faqs/:id', access_users.authentication, faqsController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;