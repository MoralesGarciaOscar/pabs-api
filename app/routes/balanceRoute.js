var express = require('express'),
    router = express.Router(),
    banlanceController = require('../controllers/balanceController'),
    access_users = require('../../middleware/access_users'),
    trafficUser = require('../../middleware/trafficUser');


router.post('/balances', [trafficUser.save, access_users.authentication], banlanceController.create);
// router.get('/banlances', access_users.authentication, banlanceController.read )
router.get('/balances/:id', [trafficUser.save, access_users.authentication], banlanceController.read)
router.get('/balances/:id/pdf', [trafficUser.save, access_users.authentication], banlanceController.createPDF)
router.get('/balances', [trafficUser.save, access_users.authentication], banlanceController.readAll);
router.put('/balances/:id', [trafficUser.save, access_users.authentication], banlanceController.update);
router.delete('/balances/:id', [trafficUser.save, access_users.authentication], banlanceController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;