const express = require('express'),
    router = express.Router(),
    notificationController = require('../controllers/notificacionController'),
    access_users = require('../../middleware/access_users'),
    trafficUser = require('../../middleware/trafficUser');


router.post('/notifications', [trafficUser.save, access_users.authentication], notificationController.create);
router.get('/notifications', [trafficUser.save, access_users.authentication], notificationController.readAll);
router.get('/notifications/:id', [trafficUser.save, access_users.authentication], notificationController.read)
router.put('/notifications/:id', [trafficUser.save, access_users.authentication], notificationController.update);
router.delete('/notifications/:id', [trafficUser.save, access_users.authentication], notificationController.delete);

//endoint by QueryString
// ........
//...HERE..
///........
module.exports = router;