'use strict'
const Sequelize = require('sequelize');
const ENV = process.env;
// Create a conexion config
const conexion = new Sequelize(ENV.DB_NAME, ENV.DB_USER, ENV.DB_PASSWORD, {
    host: ENV.DB_HOST,
    dialect: ENV.DB_DIALECT,
    port: ENV.DB_PORT,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

// sequelize.authenticate().then(function(errors) { console.log(errors) });
async function testConecction() {
    const sequelize = new Sequelize(ENV.DB_NAME, ENV.DB_USER, ENV.DB_PASSWORD, {
        host: ENV.DB_HOST,
        dialect: ENV.DB_DIALECT,
        port: ENV.DB_PORT,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });
    try {
        await sequelize.authenticate().then(() => {
            console.log('Connection has been established successfully.');
        });
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}
// testConecction();
module.exports = conexion;