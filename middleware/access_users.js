const express = require("express"),
    app = express(),
    jwt = require("jsonwebtoken");
const access_users = {};
//=========== Set Key jwt ===================
const ENV = process.env; // set environment variables
app.set("jtw_key", ENV.JWT_KEY);
app.set("public_key", ENV.JWT_PUBLIC_KEY);
app.set("private_key", ENV.JWT_PRIVATE_KEY);

access_users.getToken = ({ name, email, user_id, contract_id, first_time }) => {
    const payload = {
        check: true,
        dataUser: { email, user_id, contract_id },
    };
    log("Usuario logeado", payload);
    const token = jwt.sign(payload, app.get("jtw_key"), {
        expiresIn: "30 days",
    });
    first_time = !first_time;
    return {
        mensaje: "Correct authentication",
        token: token,
        dataUser: { name, email, user_id, contract_id, first_time },
    };
};

access_users.distroyToken = (req) => {
    const token = req.headers["authorization"];
    jwt.destroy(token);
    log("Usuario deslogeado", { message: "Usuario deslogeado", token });
    return true;
};

access_users.authentication = (req, res, next) => {
    let method = req.route.stack.length > 0 ? req.route.stack[0].method : " unknow ";
    log("HHTP Method | " + method.toUpperCase() + " | Route  | ", req.route.path);
    const token = req.headers["authorization"];
    if (token) {
        jwt.verify(token, app.get("jtw_key"), (error, decoded) => {
            if (error) {
                sendError(res, error, "Invalid token", 401);
            } else {
                req.decoded = decoded;
                log('User', req.decoded.dataUser);
                next();
            }
        });
    } else {
        sendError(res, "Error: you need a correct token", "No token provided", 401);
    }
};

module.exports = access_users;