const trafficUser = {};
trafficUser.save = (req, res, next) => {
    const token = req.headers['authorization'];
    let method = (req.route.stack.length > 0) ? req.route.stack[0].method : ' unknow ';
    log('HHTP Method | ' + method.toUpperCase() + (token ? ' with token' : ' without token') + ' | Route  | ', req.route.path);
    next();
}

module.exports = trafficUser;